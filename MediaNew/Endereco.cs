﻿using System;

public class Endereco
{
    public virtual int Id { get; protected set; }

    public virtual string Rua { get; protected set; }

    public virtual string Numero { get; protected set; }

    public virtual string Bairro { get; protected set; }

    public virtual string Cidade { get; protected set; }

    public virtual string Cep { get; protected set; }

    public virtual Uf Uf { get; protected set; }


    public Endereco(string rua, string numero, string bairro, string cidade, string cep, Uf uf)
	{
        SetRua(rua);
        SetNumero(numero);
        SetBairro(bairro);
        SetCidade(cidade);
        SetCep(cep);
        SetUf(uf);
	}

    public virtual void SetRua(string rua)
    {
        this.Rua = rua;
    }

    public virtual void SetRua(string numero)
    {
        this.Numero = numero;
    }

    public virtual void SetRua(string bairro)
    {
        this.Bairro = bairro;
    }

    public virtual void SetRua(string cidade)
    {
        this.Cidade = cidade;
    }

    public virtual void SetRua(string Cep)
    {
        this.Cep = cep;
    }

    public virtual void SetRua(Uf uf)
    {
        this.Uf = uf ?? throw new Exception("UF é obrigatório");
    }
}
