﻿using Dominio.Users.Entities;
using MediaNew.Domain.Users.Repositorios;
using MediaNew.Shared.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Units.Repositories
{
	public class UnitsRepository : BaseRepository<User>, IUsersRepository
	{
		public UnitsRepository(ISession session)
		   : base(session)
		{
		}
	}
}
