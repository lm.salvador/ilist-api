﻿using FluentNHibernate.Mapping;
using MediaNew.Domain.Units.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Units.Mappings
{
	public class UnitMap : ClassMap<Unit>
	{
		public UnitMap()
		{
			Schema("alpha");
			Table("unit");

			Id(x => x.Id).Column("id").GeneratedBy.Sequence("usuarios_id_seq");
			Map(x => x.Name).Column("name");
			Map(x => x.Abbreviation).Column("abbreviation");
		}
	}
}
