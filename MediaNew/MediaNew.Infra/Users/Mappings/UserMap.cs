﻿using Dominio.Users.Entities;
using FluentNHibernate.Mapping;

namespace MediaNew.Infra.Users.Mapeamentos
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Schema("profile");
            Table("users");

            Id(x => x.Id).Column("id").GeneratedBy.Sequence("usuarios_id_seq");
            Map(x => x.FirstName).Column("first_name");
            Map(x => x.LastName).Column("last_name");
            Map(x => x.Email).Column("email");
        }
    }
}
