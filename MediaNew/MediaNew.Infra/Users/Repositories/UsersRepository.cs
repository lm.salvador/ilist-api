﻿using Dominio.Users.Entities;
using MediaNew.Domain.Users.Repositorios;
using MediaNew.Shared.Repositories;
using NHibernate;

namespace MediaNew.Infra.Users.Repositorios
{
    public class UsersRepository : BaseRepository<User>, IUsersRepository
    {
        public UsersRepository(ISession session)
            : base(session)
        {
        }
    }
}
