﻿using AutoMapper;
using Dominio.Users.Entities;
using MediaNew.DataTransfer.Users.Responses;

namespace MediaNew.Infra.Users.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserResponse>();
        }
    }
}
