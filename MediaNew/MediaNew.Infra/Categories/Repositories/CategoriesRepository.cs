﻿using Dominio.Users.Entities;
using MediaNew.Domain.Users.Repositorios;
using MediaNew.Shared.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Categories.Repositories
{
	public class CategoriesRepository : BaseRepository<User>, IUsersRepository
	{
		public CategoriesRepository(ISession session)
			: base(session)
		{
		}
	}
}
