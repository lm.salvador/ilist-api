﻿using Dominio.Users.Entities;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Categories.Mappings
{
	public class CategoryMap : ClassMap<User>
	{
		public CategoryMap()
		{
			Schema("alpha");
			Table("category");

			Id(x => x.Id).Column("id").GeneratedBy.Sequence("usuarios_id_seq");
			Map(x => x.FirstName).Column("name");
		}
	}
}
