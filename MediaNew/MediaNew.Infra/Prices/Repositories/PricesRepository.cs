﻿using MediaNew.Domain.Prices.Entities;
using MediaNew.Domain.Prices.Repositories;
using MediaNew.Shared.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Prices.Repositories
{
	public class PricesRepository : BaseRepository<Price>, IPricesRepository
	{
		public PricesRepository(ISession session)
			: base(session)
		{
		}
	}
}
