﻿using FluentNHibernate.Mapping;
using MediaNew.Domain.Prices.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Prices.Mappings
{
	public class PriceMap : ClassMap<Price>
	{
		public PriceMap()
		{
			Schema("alpha");
			Table("price");

			Id(x => x.Id).Column("id").GeneratedBy.Sequence("usuarios_id_seq");
			Map(x => x.Date).Column("date");
			Map(x => x.Value).Column("value");
		}
	}
}
