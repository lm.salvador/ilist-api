﻿using FluentNHibernate.Mapping;
using MediaNew.Domain.Products.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Products.Mappings
{
	public class ProductMap : ClassMap<Product>
	{
		public ProductMap()
		{
			Schema("alpha");
			Table("product");

			Id(x => x.Id).Column("id").GeneratedBy.Sequence("usuarios_id_seq");
			References(x => x.Category).Column("idcategory");
			References(x => x.Unit).Column("idunit");
			References(x => x.Price).Column("idprice");
			Map(x => x.Name).Column("amount");
		}
	}
}
