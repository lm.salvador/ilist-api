﻿using MediaNew.Domain.Products.Entities;
using MediaNew.Domain.Products.Repositories;
using MediaNew.Shared.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Infra.Products.Repositories
{
	public class ProductsRepository : BaseRepository<Product>, IProductsRepository
	{
		public ProductsRepository(ISession session)
			: base(session)
		{
		}
	}
}
