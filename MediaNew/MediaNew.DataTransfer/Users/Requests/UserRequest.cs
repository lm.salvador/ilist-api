﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.DataTransfer.Users.Requests
{
    public class UserRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
