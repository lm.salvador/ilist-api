﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.DataTransfer.Users.Responses
{
    public class UserResponse
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
