﻿using System;

public class Empresa
{
    public virtual int Id { get; protected set; }

    public virtual string RazaoSocial { get; protected set; }

    public virtual string NomeFantasia { get; protected set; }

    public virtual string Cnpj { get; protected set; }

    public virtual string Email { get; protected set; }

    public virtual Endereco Endereco { get; set; }

    public Empresa(string razaoSocial, string nomeFantasia, string cnpj, string email, Endereco endereco)
    {
        SetRazaoSocial(razaoSocial);
        SetNomeFantasia(nomeFantasia);
        SetCnpj(cnpj);
        SetEmail(email);
        SetEndereco(endereco);
    }

    public virtual void SetRazaoSocial(string razaoSocial)
    {
        this.RazaoSocial = razaoSocial;
    }

    public virtual void SetNomeFantasia(string nomeFantasia)
    {
        this.NomeFantasia = nomeFantasia;
    }

    public virtual void SetCnpj(string cnpj)
    {
        this.Cnpj = cnpj;
    }

    public virtual void SetRazaoSocial(string email)
    {
        this.Email = email;
    }

    public virtual void SetRazaoSocial(Endereco endereco)
    {
        this.Endereco = endereco ?? throw new Exception("Endereço é obrigatório.");
    }
}
