﻿using AutoMapper;
using Dominio.Users.Entities;
using MediaNew.Application.Auth.Interfaces;
using MediaNew.DataTransfer.Auth.Requests;
using MediaNew.DataTransfer.Users.Requests;
using MediaNew.DataTransfer.Users.Responses;
using MediaNew.Domain.Auth.Services;
using MediaNew.Domain.Users.Repositorios;
using MediaNew.Shared.Application.Interfaces;
using System.Collections.Generic;

namespace MediaNew.Application.Auth
{
    public class AuthApplication : IAuthApplication
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IUsersRepository usersRepository;

        public AuthApplication(
            IUnitOfWork unitOfWork,
            IMapper mapper, 
            IUsersRepository usersRepository)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.usersRepository = usersRepository;
        }

        public UserResponse Create(UserRequest userRequest)
        {
            try
            {
                unitOfWork.BeginTransaction();

                var user = new User(userRequest.FirstName, userRequest.LastName, userRequest.Email, "senha", "123.456.789-09", "(27) 98129-7166", true);

                usersRepository.Insert(user);

                unitOfWork.Commit();

                return mapper.Map<UserResponse>(user);
            }
            catch
            {
                unitOfWork.Rollback();
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                unitOfWork.BeginTransaction();

                var user = usersRepository.GetById(id);

                usersRepository.Delete(user);

                unitOfWork.Commit();
            }
            catch
            {
                unitOfWork.Rollback();
                throw;
            }
        }

        public UserResponse GetUser(int id)
        {
            return mapper.Map<UserResponse>(usersRepository.GetById(id));
        }

        public IEnumerable<UserResponse> GetUsers()
        {
            return mapper.Map<IEnumerable<UserResponse>>(usersRepository.Query());
        }

        public UserResponse Update(int id, UserRequest userRequest)
        {
            try
            {
                unitOfWork.BeginTransaction();

                var user = usersRepository.GetById(id);

                user.SetFirstName(userRequest.FirstName);
                user.SetLastName(userRequest.LastName);
                user.SetEmail(userRequest.Email);
                user.SetSenha("senha");
                // user.SetCpf("123.456.789-09");
                // user.SetCelular("(27) 98129-7166");
                // user.SetAtivo(true);

                usersRepository.Update(user);

                unitOfWork.Commit();

                return mapper.Map<UserResponse>(user);
            }
            catch
            {
                unitOfWork.Rollback();
                throw;
            }
        }

        public dynamic Login(LoginRequest request)
        {
            var user = new User("Rafael", "Laranja", "r.laranja@gmail.com", "xxx", "xxx", "xx", true);

            return new
            {
                User = user,
                Token = TokenService.GenerateToken(user)
            };
        }
    }
}
