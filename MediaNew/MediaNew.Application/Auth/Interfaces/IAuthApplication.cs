﻿using MediaNew.DataTransfer.Auth.Requests;
using MediaNew.DataTransfer.Users.Requests;
using MediaNew.DataTransfer.Users.Responses;
using System.Collections.Generic;

namespace MediaNew.Application.Auth.Interfaces
{
    public interface IAuthApplication
    {
        IEnumerable<UserResponse> GetUsers();
        UserResponse GetUser(int id);
        UserResponse Create(UserRequest userRequest);
        UserResponse Update(int id, UserRequest userRequest);
        void Delete(int id);

        dynamic Login(LoginRequest request);
    }
}
