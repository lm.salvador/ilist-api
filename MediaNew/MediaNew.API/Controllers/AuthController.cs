﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediaNew.Application.Auth.Interfaces;
using MediaNew.DataTransfer.Auth.Requests;
using MediaNew.DataTransfer.Users.Requests;
using MediaNew.DataTransfer.Users.Responses;
using Microsoft.AspNetCore.Mvc;

namespace MediaNew.API.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthApplication authAppService;

        public AuthController(IAuthApplication authAppService)
        {
            this.authAppService = authAppService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<UserResponse>> Get()
        {
            return Ok(authAppService.GetUsers());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<UserResponse> Get(int id)
        {
            return Ok(authAppService.GetUser(id));
        }

        // POST api/values
        [HttpPost]
        public ActionResult<UserResponse> Post([FromBody] UserRequest request)
        {
            return Ok(authAppService.Create(request));
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult<UserResponse> Put(int id, [FromBody] UserRequest request)
        {
            return Ok(authAppService.Update(id, request));
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            authAppService.Delete(id);
        }

        [HttpPost]
        [Route("login")]
        public ActionResult<dynamic> Login([FromBody] LoginRequest request)
        {
            return Ok(authAppService.Login(request));
        }
    }
}
