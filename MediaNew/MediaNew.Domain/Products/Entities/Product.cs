﻿using MediaNew.Domain.Categories.Entities;
using MediaNew.Domain.Prices.Entities;
using MediaNew.Domain.Units.Entities;
using MediaNew.Shared.Domain;
using MediaNew.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Domain.Products.Entities
{
	public class Product : BaseEntity
	{
		public virtual Category Category { get; protected set; }

		public virtual Unit Unit { get; protected set; }

		public virtual Price Price { get; protected set; }

		public virtual string Name { get; protected set; }

		public virtual int Amount { get; protected set; }

		protected Product() {}

		public Product(Category category, Unit unit, Price price, string name, int amount)
		{
			SetCategory(category);
			SetUnit(unit);
			SetPrice(price);
			SetName(name);
			SetQuantity(amount);
		}

		public virtual void SetCategory(Category category)
		{
			this.Category = category ?? throw new RequiredAttributeException("Category");
		}

		public virtual void SetUnit(Unit unit)
		{
			this.Unit = unit ?? throw new RequiredAttributeException("Unit");
		}

		public virtual void SetPrice(Price price)
		{
			this.Price = price ?? throw new RequiredAttributeException("Price");
		}

		public virtual void SetName(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new RequiredAttributeException("Name");
			}

			this.Name = name;
		}

		public virtual void SetQuantity(int amount)
		{
			this.Amount = amount;
		}
	}
}
