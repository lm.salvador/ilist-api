﻿using MediaNew.Domain.Products.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaNew.Domain.Products.Repositories
{
	public interface IProductsRepository
	{
		IQueryable<Product> Query();
		Product GetById(int id);
		void Insert(Product product);
		void Update(Product product);
		void Delete(Product product);
	}
}
