﻿using Dominio.Users.Entities;
using System.Linq;

namespace MediaNew.Domain.Users.Repositorios
{
    public interface IUsersRepository
    {
        IQueryable<User> Query();
        User GetById(int id);
        void Insert(User user);
        void Update(User user);
        void Delete(User user);
    }
}
