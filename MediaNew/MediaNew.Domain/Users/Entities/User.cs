﻿using MediaNew.Shared.Domain;
using MediaNew.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Users.Entities
{
    public class User : BaseEntity
    {
        public virtual string FirstName { get; protected set; }

        public virtual string LastName { get; protected set; }

        public virtual string Email { get; protected set; }

        public virtual string Senha { get; protected set; }

        public virtual string Cpf { get; protected set; }

        public virtual string Celular { get; protected set; }

        public virtual bool Ativo { get; protected set; }

        protected User()
        {

        }

        public User(string firstName, string lastName, string email, string senha, string cpf, string celular, bool ativo)
        {
            SetFirstName(firstName);
            SetLastName(lastName);
            SetEmail(email);
            SetSenha(senha);
            SetCpf(cpf);
            SetCelular(celular);
            SetAtivo(ativo);
        }

        public virtual void SetFirstName(string firstName)
        {
            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new RequiredAttributeException("Primeiro nome");
            }

            this.FirstName = firstName;
        }

        public virtual void SetLastName(string lastName)
        {
            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new RequiredAttributeException("Ultimo nome é necessário");
            }

            this.LastName = lastName;
        }

        public virtual void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new RequiredAttributeException("Email é necessário.");
            }

            this.Email = email;
        }

        public virtual void SetSenha(string senha)
        {
            if (string.IsNullOrWhiteSpace(senha))
            {
                throw new RequiredAttributeException("Necessário ter uma senha.");
            }

            this.Senha = senha;
        }

        public virtual void SetCpf(string cpf)
        {
            if (string.IsNullOrWhiteSpace(cpf))
            {
                throw new RequiredAttributeException("CPF é necessário");
            }

            this.Cpf = cpf;
        }

        public virtual void SetCelular(string celular)
        {
            if (string.IsNullOrWhiteSpace(celular))
            {
                throw new RequiredAttributeException("Celular é necessário");
            }

            this.Celular = celular;
        }

        public virtual void SetAtivo(bool ativo)
        {
            this.Ativo = ativo;
        }

        public virtual bool CheckPassword(string password)
        {
            return this.Senha == password;
        }
    }
}
