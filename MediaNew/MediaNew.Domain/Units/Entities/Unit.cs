﻿using MediaNew.Shared.Domain;
using MediaNew.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Domain.Units.Entities
{
	public class Unit : BaseEntity
	{
		public virtual string Name { get; protected set; }

		public virtual string Abbreviation { get; protected set; }

		protected Unit(){}

		public Unit(string name, string abbreviation)
		{
			SetName(name);
			SetAbbreviation(abbreviation);
		}

		public virtual void SetName(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new RequiredAttributeException("Nome");
			}

			this.Name = name;
		}

		public virtual void SetAbbreviation(string abbreviation)
		{
			if (string.IsNullOrWhiteSpace(abbreviation))
			{
				throw new RequiredAttributeException("Abreviação");
			}

			this.Abbreviation = abbreviation;
		}
	}
}
