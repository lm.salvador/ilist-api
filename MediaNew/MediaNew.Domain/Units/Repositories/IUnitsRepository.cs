﻿using MediaNew.Domain.Units.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaNew.Domain.Units.Repositories
{
	public interface IUnitsRepository
	{
		IQueryable<Unit> Query();
		Unit GetById(int id);
		void Insert(Unit unit);
		void Update(Unit unit);
		void Delete(Unit unit);
	}
}
