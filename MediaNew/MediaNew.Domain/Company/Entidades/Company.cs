﻿using Dominio.Enderecos.Entidades;
using MediaNew.Shared.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Empresas.Entidades
{
    public class Company : BaseEntity
    { 
        public virtual string CompanyName { get; protected set; }

        public virtual string FantasyName { get; protected set; }

        public virtual string Cnpj { get; protected set; }

        public virtual string Email { get; protected set; }

        public virtual Endereco Endereco { get; protected set; }

        public Company(string razaoSocial, string nomeFantasia, string cnpj, string email, Endereco endereco)
        {
            SetCompanyName(razaoSocial);
            SetFantasyName(nomeFantasia);
            SetCnpj(cnpj);
            SetEmail(email);
            SetEndereco(endereco);
        }

        public virtual void SetCompanyName(string companyName)
        {
            this.CompanyName = companyName;
        }

        public virtual void SetFantasyName(string fantasyName)
        {
            this.FantasyName = fantasyName;
        }

        public virtual void SetCnpj(string cnpj)
        {
            this.Cnpj = cnpj;
        }

        public virtual void SetEmail(string email)
        {
            this.Email = email;
        }

        public virtual void SetEndereco(Endereco endereco)
        {
            this.Endereco = endereco ?? throw new Exception("Endereço é obrigatório.");
        }
    }
}
