﻿using MediaNew.Shared.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Ufs.Entidades
{
    public class FederatedUnit : BaseEntity
    {

        public virtual string Abbreviation { get; protected set; }

        public virtual string State { get; protected set; }

        public FederatedUnit(string abreviacao, string estado)
        {
            SetAbbreviation(abreviacao);
            SetState(estado);
        }

        public virtual void SetAbbreviation(string abbreviation)
        {
            this.Abbreviation = abbreviation;
        }

        public virtual void SetState(string state)
        {
            this.State = state;
        }
    }
}
