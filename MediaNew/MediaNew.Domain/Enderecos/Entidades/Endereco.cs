﻿using System;
using System.Collections.Generic;
using System.Text;
using Dominio.Ufs.Entidades;

namespace Dominio.Enderecos.Entidades
{
    public class Endereco
    {
        public virtual int Id { get; protected set; }

        public virtual string Rua { get; protected set; }

        public virtual string Numero { get; protected set; }

        public virtual string Bairro { get; protected set; }

        public virtual string Cidade { get; protected set; }

        public virtual string Cep { get; protected set; }

        public virtual FederatedUnit FederatedUnit { get; protected set; }


        public Endereco(string rua, string numero, string bairro, string cidade, string cep, FederatedUnit federatedUnit)
        {
            SetRua(rua);
            SetNumero(numero);
            SetBairro(bairro);
            SetCidade(cidade);
            SetCep(cep);
			SetFederatedUnit(federatedUnit);
        }

        public virtual void SetRua(string rua)
        {
            this.Rua = rua;
        }

        public virtual void SetNumero(string numero)
        {
            this.Numero = numero;
        }

        public virtual void SetBairro(string bairro)
        {
            this.Bairro = bairro;
        }

        public virtual void SetCidade(string cidade)
        {
            this.Cidade = cidade;
        }

        public virtual void SetCep(string cep)
        {
            this.Cep = cep;
        }

        public virtual void SetFederatedUnit(FederatedUnit federatedUnit)
        {
            this.FederatedUnit = federatedUnit ?? throw new Exception("UF é obrigatório");
        }
    }
}
