﻿using MediaNew.Shared.Domain;
using MediaNew.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Domain.Categories.Entities
{
	public class Category : BaseEntity
	{
		public virtual string Name { get; protected set; }

		protected Category() { }

		public Category(string name)
		{
			SetName(name);
		}

		public virtual void SetName(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new RequiredAttributeException("Nome");
			}

			this.Name = name;
		}
	}
}
