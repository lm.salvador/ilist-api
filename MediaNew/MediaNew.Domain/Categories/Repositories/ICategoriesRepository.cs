﻿using MediaNew.Domain.Categories.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaNew.Domain.Categories.Repositories
{
	public interface ICategoriesRepository
	{
		IQueryable<Category> Query();
		Category GetById(int id);
		void Insert(Category category);
		void Update(Category category);
		void Delete(Category category);
	}
}
