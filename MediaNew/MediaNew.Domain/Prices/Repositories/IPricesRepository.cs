﻿using MediaNew.Domain.Prices.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MediaNew.Domain.Prices.Repositories
{
	public interface IPricesRepository
	{
		IQueryable<Price> Query();
		Price GetById(int id);
		void Insert(Price price);
		void Update(Price price);
		void Delete(Price price);
	}
}
