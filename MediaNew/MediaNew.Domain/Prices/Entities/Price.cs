﻿using MediaNew.Shared.Domain;
using MediaNew.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MediaNew.Domain.Prices.Entities
{
	public class Price : BaseEntity
	{
		public virtual DateTime Date { get; protected set; }

		public virtual float Value { get; protected set; }

		protected Price()
		{

		}

		public Price(DateTime date, float value)
		{
			SetDate(date);
			SetValue(value);
		}

		public virtual void SetDate(DateTime date)
		{
			this.Date = date;
		}

		public virtual void SetValue(float value)
		{
			if (value < 0)
			{
				throw new NegativeAttributeException();
			}

			this.Value = value;
		}
	}
}
