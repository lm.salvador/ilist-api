﻿using System;

public class Usuario
{
    public virtual int Id { get; protected set; }

    public virtual string Nome { get; protected set; }

    public virtual string Email { get; protected set; }

    public virtual string Senha { get; protected set; }

    public virtual string Cpf { get; protected set; }

    public virtual string Celular { get; protected set; }

    public virtual bool Ativo { get; protected set; }

    public Usuario(string nome, string email, string senha, string cpf, string celular, bool ativo)
    {
        SetNome(nome);
        SetEmail(email);
        SetSenha(senha);
        SetCpf(cpf);
        SetCelular(celular);
        SetAtivo(ativo);
    }

    public virtual void SetNome(string nome)
    {
        if (string.IsNullOrWhiteSpace(nome))
        {
            throw new ArgumentException("Nome é necessário", nameof(nome));
        }

        this.Nome = nome;
    }

    public virtual void SetEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
        {
            throw new ArgumentException("Email é necessário.", nameof(email));
        }

        this.Email = email;
    }

    public virtual void SetSenha(string senha)
    {
        if (string.IsNullOrWhiteSpace(senha))
        {
            throw new ArgumentException("Necessário ter uma senha.", nameof(senha));
        }

        this.Senha = senha;
    }

    public virtual void SetEmail(string cpf)
    {
        if (string.IsNullOrWhiteSpace(cpf))
        {
            throw new ArgumentException("CPF é necessário", nameof(cpf));
        }

        this.Cpf = cpf;
    }

    public virtual void SetEmail(string celular)
    {
        if (string.IsNullOrWhiteSpace(celular))
        {
            throw new ArgumentException("Celular é necessário", nameof(celular));
        }

        this.Celular = celular;
    }

    public virtual void SetEmail(bool ativo)
    {
        this.Ativo = ativo;
    }
}
