﻿using AutoMapper;
using MediaNew.Application.Auth;
using MediaNew.Application.Auth.Interfaces;
using MediaNew.Domain.Users.Repositorios;
using MediaNew.Infra.Users.Profiles;
using MediaNew.Infra.Users.Repositorios;
using MediaNew.Ioc.Providers;
using MediaNew.Shared.Application;
using MediaNew.Shared.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace MediaNew.Ioc
{
    public static class Ioc
    {
        public static void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            // NHibernate
            services.AddSingleton(NHibernateProvider.ConfigurarDatabase(configuration.GetSection("ConnectionStrings").GetSection("Main").Value).OpenSession());

            // Automapper
            services.AddAutoMapper(Assembly.GetAssembly(typeof(UserProfile)));

            // Unit of work
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            // AppServico
            services.AddTransient<IAuthApplication, AuthApplication>();

            // Repositorios
            services.AddTransient<IUsersRepository, UsersRepository>();
        }
    }
}
