﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using MediaNew.Infra.Users.Mapeamentos;
using NHibernate;
using System.Reflection;

namespace MediaNew.Ioc.Providers
{
    public static class NHibernateProvider
    {
        public static ISessionFactory ConfigurarDatabase(string connectionString)
        {
            return Fluently
                        .Configure()
                        .Database(PostgreSQLConfiguration.Standard.ConnectionString(connectionString))
                        .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(UserMap))))
                        .BuildSessionFactory();
        }
    }
}
