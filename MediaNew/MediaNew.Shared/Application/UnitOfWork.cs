﻿using MediaNew.Shared.Application.Interfaces;
using NHibernate;

namespace MediaNew.Shared.Application
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISession session;
        private ITransaction transaction;

        public UnitOfWork(ISession session)
        {
            this.session = session;
        }

        public void BeginTransaction()
        {
            transaction = session.BeginTransaction();
        }

        public void Commit()
        {
            transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }
    }
}
