﻿using MediaNew.Shared.Domain;
using NHibernate;
using System.Linq;

namespace MediaNew.Shared.Repositories
{
    public class BaseRepository<T> where T : BaseEntity
    {
        protected readonly ISession session;

        public BaseRepository(ISession session)
        {
            this.session = session;
        }

        public IQueryable<T> Query()
        {
            return session.Query<T>();
        }

        public T GetById(int id)
        {
            return session.Query<T>().SingleOrDefault(x => x.Id == id);
        }

        public void Insert(T instance)
        {
            session.Save(instance);
        }

        public void Update(T instance)
        {
            session.Update(instance);
        }

        public void Delete(T instance)
        {
            session.Delete(instance);
        }
    }
}
