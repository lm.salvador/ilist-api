﻿namespace MediaNew.Shared.Domain
{
    public class BaseEntity
    {
        public virtual int Id { get; protected set; }
    }
}
