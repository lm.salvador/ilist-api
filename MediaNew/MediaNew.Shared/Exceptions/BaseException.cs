﻿using System;

namespace MediaNew.Shared.Exceptions
{
    public class BaseException : Exception
    {
        public BaseException(string message)
            : base(message)
        {
        }
    }
}
