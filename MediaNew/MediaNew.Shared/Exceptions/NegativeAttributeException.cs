﻿
namespace MediaNew.Shared.Exceptions
{
	public class NegativeAttributeException : BaseException
	{
		public NegativeAttributeException()
		   : base("Valor não pode ser menor que 0")
		{
		}
	}
}
