﻿
namespace MediaNew.Shared.Exceptions
{
    public class RequiredAttributeException : BaseException
    {
        public RequiredAttributeException(string atributo)
            : base($"{atributo} é obrigatório")
        {
        }
    }
}
